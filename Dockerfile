# Use an official Node.js runtime as the base image
FROM node:latest

# Set the working directory inside the container
WORKDIR /frontend

# Copy package.json and yarn.lock (or package-lock.json) to the container
COPY . ./

# Install dependencies
RUN yarn install

# Expose the port your Node.js app is listening on
EXPOSE 3001

# Command to start your Node.js app
CMD ["yarn", "dev"]